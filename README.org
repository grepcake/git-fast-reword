#+title: git-fast-reword

Reword commit messages without modifying your file tree.

* Installation
  - install =python3=
  - go to the project root
  - (optionally) create a virtual environment, so as not to clutter
    global python installation
  - =pip install -r requirements.txt=

* Usage
  - switch to the desired git repository
  - run =python3 /path/to/main.py --help= to see available options

    /(=/path/to/main.py=) is a path to [[file:main.py]] from this repo/

* Examples
  - =python3 /path/to/main.py one-commit -m 'fix typo' HEAD~4=
  - =python3 /path/to/main.py from-csv mapping.csv=

    /mapping.csv:/
    #+begin_src
    f8d06b5,no-space
    1c53222,"long, detailed message

    over several lines
    "
    #+end_src

  - =python3 /path/to/main.py --use-filter-repo one-commit -m 'msg' 53c8aa2=


* Testing
  - =cd t=
  - =sh ./test-all.sh=

* Explanation
  Explanation of how this works may be found in Russian in [[file:explanation.org]].
